#!/usr/bin/python
# 
# CIS 472/572 -- Programming Homework #1
#
# Starter code provided by Daniel Lowd, 1/20/2020
# Please use this template as a starting point, 
# to support autograding with Gradescope.
'''
This homework has completed by Daniel Loyd and Will Christensen
'''
import argparse
import numpy as np

# Process arguments for k-NN classification
def handle_args():
    parser = argparse.ArgumentParser(description=
                 'Make predictions using the k-NN algorithms.')

    parser.add_argument('-k', type=int, default=1, help='Number of nearest neighbors to consider')
    parser.add_argument('--varnorm', action='store_true', help='Normalize features to zero mean and unit variance')
    parser.add_argument('--rangenorm', action='store_true', help='Normalize features to the range [-1,+1]')
    parser.add_argument('--exnorm', action='store_true', help='Normalize examples to unit length')
    parser.add_argument('train',  help='Training data file')
    parser.add_argument('test',   help='Test data file')

    return parser.parse_args()


# Load data from a file
def read_data(filename):
  data = np.genfromtxt(filename, delimiter=',', skip_header=1)
  x = data[:, 0:-1]
  y = data[:, -1]
  return (x,y)


# Distance between instances x1 and x2
def dist(x1, x2):
  temp = x1 - x2
  distance = np.sum(np.power(temp, 2))
  return distance

# Predict label for instance x, using k nearest neighbors in training data
def classify(train_x, train_y, k, x):
  distances = np.array([dist(x, data) for data in train_x])
  indexes = distances.argsort()[:k]
  y_list = [train_y[index] for index in indexes]
  ret = max(set(y_list), key=y_list.count)

  return ret


# Process the data to normalize features and/or examples.
# NOTE: You need to normalize both train and test data the same way.
def normalize_data(train_x, test_x, rangenorm, varnorm, exnorm):
  if rangenorm:
    def rangenorm(data):
      find_max = np.max(train_x, axis=0)
      find_min = np.min(train_x, axis=0)
      return 2*((data - find_min)/(find_max - find_min)) - 1

    train_x = [rangenorm(data) for data in train_x]
    test_x = [rangenorm(data) for data in test_x]

  if varnorm:
    def varnorm(data):
      zero_mean = np.subtract(data, np.mean(train_x, axis=0))
      unit_variance = np.std(train_x, axis=0)
      return np.divide(zero_mean, unit_variance)

    train_x = varnorm(train_x)
    test_x = varnorm(test_x)

  if exnorm:
    def exnorm(data):
      squares = np.power(train_x, 2)
      norm = np.sum(squares)
      norm = pow(norm, 0.5)
      return np.divide(data, norm)

    train_x = exnorm(train_x)
    test_x = exnorm(test_x)

  return train_x, test_x


# Run classifier and compute accuracy
def runTest(test_x, test_y, train_x, train_y, k):
  correct = 0
  
  for (x,y) in zip(test_x, test_y):
    if classify(train_x, train_y, k, x) == y:
      correct += 1
    
  acc = float(correct)/len(test_x)
  return acc


# Load train and test data.  Learn model.  Report accuracy.
# (NOTE: You shouldn't need to change this.)
def main():

  args = handle_args()

  # Read in lists of examples.  Each example is a list of attribute values,
  # where the last element in the list is the class value.
  (train_x, train_y) = read_data(args.train)
  (test_x, test_y)   = read_data(args.test)
  # Normalize the training data
  (train_x, test_x) = normalize_data(train_x, test_x, 
                          args.rangenorm, args.varnorm, args.exnorm)
  acc = runTest(test_x, test_y,train_x, train_y,args.k)
  print("Accuracy: ",acc)

if __name__ == "__main__":
  main()
